# IRIS #



### What is IRIS? ###

**IRIS** (**I**ncident **R**esiliency & **I**nfrastructure **S**ecurity) is an implementation of **Azure Sentinel** that focuses on healthcare cybersecurity.